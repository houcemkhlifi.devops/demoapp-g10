# Etage 1
FROM node:16-alpine3.12 as build

# Definition du repertoire de travail
WORKDIR /exo1

# Copie du projet dans le repertoire de travail
COPY . .

# Installation des packages
RUN npm install --force

# Build de l'application
RUN npm run build -- --outputPath=./dist/out --configuration=production

# Etage2
FROM nginx:alpine

# Déploiement de l'application
COPY --from=build /exo1/dist/out /usr/share/nginx/html

EXPOSE 80
